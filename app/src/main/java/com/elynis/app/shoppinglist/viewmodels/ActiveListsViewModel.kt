package com.elynis.app.shoppinglist.viewmodels

import androidx.lifecycle.*
import com.elynis.app.shoppinglist.data.db.entities.ShoppingList
import com.elynis.app.shoppinglist.data.db.relations.ShoppingListWithShoppingItems
import com.elynis.app.shoppinglist.data.repositories.ShoppingRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ActiveListsViewModel @Inject constructor(
    val repository: ShoppingRepository,
    private val defaultDispatcher: CoroutineDispatcher
) : ViewModel() {

    private val _currentLists = MediatorLiveData<List<ShoppingListWithShoppingItems>>()
    val currentLists: LiveData<List<ShoppingListWithShoppingItems>>
        get() = _currentLists

    private var cachedShoppingLists = listOf<ShoppingListWithShoppingItems>()
    private var isSearchStarting = true
    var isSearching = MutableLiveData(false)

    init {
        _currentLists.addSource(repository.getAllActiveShoppingListsSortedByDate()) {
            _currentLists.value = it
            cachedShoppingLists = it
        }
    }

    fun archiveShoppingList(shoppingList: ShoppingList) = viewModelScope.launch(defaultDispatcher) {
        shoppingList.isArchived = true
        insertShoppingList(shoppingList)
    }

    fun insertShoppingList(shoppingList: ShoppingList) = viewModelScope.launch(defaultDispatcher) {
        repository.insertShoppingList(shoppingList)
    }

    fun deleteShoppingList(shoppingList: ShoppingList) = viewModelScope.launch(defaultDispatcher) {
        repository.deleteShoppingList(shoppingList)
    }

    fun searchShoppingList(query: String) = viewModelScope.launch(defaultDispatcher) {
        val listToSearch = if (isSearchStarting) {
            _currentLists.value
        } else {
            cachedShoppingLists
        }
        if (query.isEmpty()) {
            _currentLists.value = cachedShoppingLists
            isSearching.value = false
            isSearchStarting = true
            return@launch
        }
        val results = listToSearch?.filter {
            it.shoppingList.name.contains(query.trim(), ignoreCase = true)
        }
        if (isSearchStarting) {
            _currentLists.value?.let { cachedShoppingLists = it }
            isSearchStarting = false
        }
        results?.let {
            _currentLists.value = it
            isSearching.value = true
        }
    }
}