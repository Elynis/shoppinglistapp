package com.elynis.app.shoppinglist.data.repositories

import androidx.lifecycle.LiveData
import com.elynis.app.shoppinglist.data.db.entities.ShoppingItem
import com.elynis.app.shoppinglist.data.db.entities.ShoppingList
import com.elynis.app.shoppinglist.data.db.relations.ShoppingListWithShoppingItems
import kotlinx.coroutines.flow.StateFlow

interface ShoppingRepository {
    suspend fun dropShoppingListsTable()
    suspend fun dropShoppingItemsTable()

    suspend fun insertShoppingItem(item: ShoppingItem)
    suspend fun deleteShoppingItem(item: ShoppingItem)

    suspend fun insertShoppingList(list: ShoppingList) : Long
    suspend fun deleteShoppingList(list: ShoppingList)

    fun getShoppingItemsOfShoppingList(id: Int) : LiveData<ShoppingListWithShoppingItems?>
    fun getAllActiveShoppingListsSortedByDate(): LiveData<List<ShoppingListWithShoppingItems>>
    fun getAllArchivedShoppingListsSortedByDate(): LiveData<List<ShoppingListWithShoppingItems>>
}