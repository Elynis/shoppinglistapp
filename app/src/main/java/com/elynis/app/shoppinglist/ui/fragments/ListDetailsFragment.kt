package com.elynis.app.shoppinglist.ui.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.ItemTouchHelper.LEFT
import androidx.recyclerview.widget.ItemTouchHelper.RIGHT
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.elynis.app.shoppinglist.R
import com.elynis.app.shoppinglist.adapters.ItemsAdapter
import com.elynis.app.shoppinglist.data.db.entities.ShoppingItem
import com.elynis.app.shoppinglist.ui.dialog.AddOrEditDialogListener
import com.elynis.app.shoppinglist.ui.dialog.AddOrEditShoppingItemDialog
import com.elynis.app.shoppinglist.viewmodels.ListDetailsViewModel
import com.elynis.app.shoppinglist.util.Constants
import com.elynis.app.shoppinglist.util.Currency
import com.elynis.app.shoppinglist.util.roundToDecimals
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_list_details.*

@AndroidEntryPoint
class ListDetailsFragment : Fragment(R.layout.fragment_list_details) {

    lateinit var itemsAdapter: ItemsAdapter
    private var isRecyclerViewSetup = false

    private val viewModel: ListDetailsViewModel by viewModels()
    private var addOrEditShoppingItemDialog: AddOrEditShoppingItemDialog? = null


    override fun onPause() {
        addOrEditShoppingItemDialog?.apply {
            if (isShowing) {
                dismiss()
            }
        }
        super.onPause()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupLiveData()
        subscribeToObservers()

        btnAdd.setOnClickListener {
            AddOrEditShoppingItemDialog(requireContext(), addOrEditDialogListener).apply {
                addOrEditShoppingItemDialog = this
                show()
            }
        }

        etListName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                s?.toString()?.let {
                    if (it != viewModel.shoppingListName.value) {
                        viewModel.setListName(it)
                    }
                }
            }
        })
    }


    private val itemTouchCallback = object : ItemTouchHelper.SimpleCallback(0, LEFT or RIGHT) {
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean = true

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            val position = viewHolder.layoutPosition
            val item = itemsAdapter.differ.currentList[position]
            viewModel.deleteShoppingItem(item)
            Snackbar.make(
                requireView(),
                getString(R.string.snackbar_item_sucessfully_deleted),
                Snackbar.LENGTH_LONG
            ).apply {
                setAction(getString(R.string.undo)) {
                    viewModel.insertShoppingItem(item)
                }
                show()
            }

        }
    }

    private val itemTouchHelper = ItemTouchHelper(itemTouchCallback)


    private fun setupLiveData() {
        viewModel.initLiveDataWithGivenShoppingListId(
            arguments?.getString(Constants.SHOPPING_LIST_ID_ARGUMENT_NAME)?.toIntOrNull()
        )
    }

    private fun subscribeToObservers() {
        viewModel.shoppingListWithShoppingItems.observe(viewLifecycleOwner) { shoppingListWithShoppingItems ->
            shoppingListWithShoppingItems?.shoppingList?.id?.let { id ->
                arguments?.putString(Constants.SHOPPING_LIST_ID_ARGUMENT_NAME, id.toString())
            }


            btnAdd.visibility = if(shoppingListWithShoppingItems.shoppingList.isArchived) View.GONE else View.VISIBLE
            tilListName.isEnabled = !shoppingListWithShoppingItems.shoppingList.isArchived

            setupAdapter(shoppingListWithShoppingItems.shoppingList.isArchived)
            itemsAdapter.submitList(shoppingListWithShoppingItems.shoppingItems)


            shoppingListWithShoppingItems.shoppingList.name.let {
                if (it != etListName.text.toString()) {
                    etListName.setText(it)
                }
            }

            val productCount = shoppingListWithShoppingItems.shoppingItems.count()
            val completedProductCount =
                shoppingListWithShoppingItems.shoppingItems.count { it.isCompleted }
            var productCountString = "$completedProductCount / $productCount "
            productCountString += if (productCount > 1 && productCount == 0) "Products" else "Product"

            tvProductCount.text = productCountString

            val priceTotal =
                shoppingListWithShoppingItems.shoppingItems.sumOf { it.cost.toDouble() }
                    .roundToDecimals(2)
            val priceTotalString = "Total: $priceTotal ${Currency.DOLAR.description}"
            tvTotal.text = priceTotalString

            if (shoppingListWithShoppingItems.shoppingItems.isEmpty()) {
                empty_view.visibility = View.VISIBLE
                rvItems.visibility = View.GONE
            } else {
                empty_view.visibility = View.GONE
                rvItems.visibility = View.VISIBLE
            }

        }
    }

    private fun setupAdapter(isArchived: Boolean) {
        if (!isRecyclerViewSetup) {
            itemsAdapter = ItemsAdapter().apply {
                if (!isArchived) {
                    setOnCheckedBtnClicked { item ->
                        item.isCompleted = !item.isCompleted
                        viewModel.insertShoppingItem(item)
                    }
                    setOnEditBtnClicked { shoppingItem ->
                        AddOrEditShoppingItemDialog(
                            requireContext(),
                            addOrEditDialogListener,
                            shoppingItem
                        ).apply {
                            addOrEditShoppingItemDialog = this
                            show()
                        }
                    }
                }
                isListArchived = isArchived
            }


            rvItems.adapter = itemsAdapter
            rvItems.layoutManager = LinearLayoutManager(requireContext())
            itemTouchHelper.attachToRecyclerView(if (isArchived) null else rvItems)
            isRecyclerViewSetup = true
        }
    }

    private val addOrEditDialogListener = object : AddOrEditDialogListener {
        override fun onBtnClicked(item: ShoppingItem) {
            item.shoppingListId = viewModel.shoppingListWithShoppingItems.value?.shoppingList?.id
            viewModel.insertShoppingItem(item)
            addOrEditShoppingItemDialog = null
        }
    }
}