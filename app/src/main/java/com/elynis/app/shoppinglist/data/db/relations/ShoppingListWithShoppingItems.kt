package com.elynis.app.shoppinglist.data.db.relations

import androidx.room.Embedded
import androidx.room.Relation
import com.elynis.app.shoppinglist.data.db.entities.ShoppingItem
import com.elynis.app.shoppinglist.data.db.entities.ShoppingList

data class ShoppingListWithShoppingItems(
    @Embedded val shoppingList: ShoppingList,
    @Relation(
        parentColumn = "id",
        entityColumn = "shoppingListId"
    )
    val shoppingItems: List<ShoppingItem>
)
