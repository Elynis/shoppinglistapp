package com.elynis.app.shoppinglist.util

enum class Currency(val description: String) {
    DOLAR("$")
}