package com.elynis.app.shoppinglist.data.db.migrations

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase


object Migrations {
    val MIGRATION_1_2: Migration = object : Migration(1, 2) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL(
                "ALTER TABLE shopping_items ADD COLUMN isCompleted INTEGER NOT NULL DEFAULT(0)"
            )
        }
    }
}