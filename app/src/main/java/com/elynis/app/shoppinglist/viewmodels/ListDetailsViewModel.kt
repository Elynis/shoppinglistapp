package com.elynis.app.shoppinglist.viewmodels

import androidx.lifecycle.*
import com.elynis.app.shoppinglist.data.db.entities.ShoppingItem
import com.elynis.app.shoppinglist.data.db.entities.ShoppingList
import com.elynis.app.shoppinglist.data.db.relations.ShoppingListWithShoppingItems
import com.elynis.app.shoppinglist.data.repositories.ShoppingRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListDetailsViewModel @Inject constructor(
    val repository: ShoppingRepository
) : ViewModel() {

    private val _shoppingListWithShoppingItems = MediatorLiveData<ShoppingListWithShoppingItems>()
    val shoppingListWithShoppingItems: LiveData<ShoppingListWithShoppingItems>
        get() = _shoppingListWithShoppingItems

    private val _shoppingList = MutableLiveData<ShoppingList>()
    val shoppingList: LiveData<ShoppingList>
        get() = _shoppingList

    private val _shoppingListId = MutableLiveData<Int>()
    val shoppingListId: LiveData<Int>
        get() = _shoppingListId

    private val _shoppingListName = MutableLiveData<String>()
    val shoppingListName: LiveData<String>
        get() = _shoppingListName

    fun setListName(newName: String) {
        _shoppingList.value?.apply {
            name = newName
        }?.also {
            insertShoppingList(it)
        }
    }

    fun initLiveDataWithGivenShoppingListId(id: Int?) {
        if (id != null) {
            _shoppingListWithShoppingItems.addSource(repository.getShoppingItemsOfShoppingList(id)) {
                it?.let {
                    _shoppingListId.value = it.shoppingList.id!!
                    _shoppingListName.value = it.shoppingList.name
                    _shoppingList.value = it.shoppingList
                }
                _shoppingListWithShoppingItems.value = it
            }
        } else {
            insertShoppingList(
                ShoppingList(
                    timestampOfCreation = System.currentTimeMillis(),
                    timestampOfLastChange = System.currentTimeMillis()
                )
            )
        }
    }

    fun insertShoppingItem(shoppingItem: ShoppingItem) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertShoppingItem(shoppingItem)
    }

    fun deleteShoppingItem(shoppingItem: ShoppingItem) = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteShoppingItem(shoppingItem)
    }

    fun insertShoppingList(shoppingList: ShoppingList) = viewModelScope.launch(Dispatchers.Main) {
        shoppingList.timestampOfLastChange = System.currentTimeMillis()
        val newId = repository.insertShoppingList(shoppingList).toInt()
        initLiveDataWithGivenShoppingListId(newId)
    }
}