package com.elynis.app.shoppinglist.data.db

import androidx.room.TypeConverter
import com.elynis.app.shoppinglist.data.db.entities.ShoppingItemUnit

class Converters {
    @TypeConverter
    fun toShoppingItemUnit(value: String) = enumValueOf<ShoppingItemUnit>(value)

    @TypeConverter
    fun fromShoppingItemUnit(value: ShoppingItemUnit) = value.name
}