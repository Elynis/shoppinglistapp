package com.elynis.app.shoppinglist.ui.dialog

import com.elynis.app.shoppinglist.data.db.entities.ShoppingItem

interface AddOrEditDialogListener {
    fun onBtnClicked(item: ShoppingItem)
}