package com.elynis.app.shoppinglist.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.elynis.app.shoppinglist.CoroutinesTestRule
import com.elynis.app.shoppinglist.data.db.entities.ShoppingList
import com.elynis.app.shoppinglist.data.db.relations.ShoppingListWithShoppingItems
import com.elynis.app.shoppinglist.data.repositories.FakeShoppingRepositoryImpl
import com.elynis.app.shoppinglist.getOrAwaitValueTest
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test


@ExperimentalCoroutinesApi
class ActiveListsViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    private lateinit var viewModel: ActiveListsViewModel

    @Before
    fun setup() {
        viewModel =
            ActiveListsViewModel(FakeShoppingRepositoryImpl(), coroutinesTestRule.testDispatcher)
    }

    @Test
    fun `search lists with empty query, returns whole list`() =
        coroutinesTestRule.testDispatcher.runBlockingTest {
            listOf(
                ShoppingList(name = "ShoppingList1"),
                ShoppingList(name = "ShoppingList2"),
                ShoppingList(name = "ShoppingList3")
            ).onEach {
                viewModel.insertShoppingList(it)
            }
            val listsBeforeSearch = viewModel.currentLists.getOrAwaitValueTest()

            viewModel.searchShoppingList("")
            val listsAfterSearch = viewModel.currentLists.getOrAwaitValueTest()

            assertThat(listsBeforeSearch).isEqualTo(listsAfterSearch)
        }

    @Test
    fun `search lists with valid query, returns filtered list`() =
        coroutinesTestRule.testDispatcher.runBlockingTest {
            val list1 = ShoppingList(name = "1")
            viewModel.insertShoppingList(list1)
            viewModel.insertShoppingList(ShoppingList(name = "2"))
            viewModel.insertShoppingList(ShoppingList(name = "3"))

            viewModel.searchShoppingList("1")
            val listsAfterSearch = viewModel.currentLists.getOrAwaitValueTest()
            val filteredList = listOf(ShoppingListWithShoppingItems(list1, emptyList()))

            assertThat(listsAfterSearch).isEqualTo(filteredList)
        }

    @Test
    fun `search lists with non-applicable query, returns empty list`() =
        coroutinesTestRule.testDispatcher.runBlockingTest {
            viewModel.insertShoppingList(ShoppingList(name = "ShoppingList1"))
            viewModel.insertShoppingList(ShoppingList(name = "ShoppingList2"))
            viewModel.insertShoppingList(ShoppingList(name = "ShoppingList3"))

            viewModel.searchShoppingList("some query")
            val listsAfterSearch = viewModel.currentLists.getOrAwaitValueTest()

            assertThat(listsAfterSearch).isEqualTo(emptyList<ShoppingListWithShoppingItems>())
        }

    @Test
    fun `list is archived, returns lists with one less item`() =
        coroutinesTestRule.testDispatcher.runBlockingTest {
            val list = ShoppingList(isArchived = false)
            viewModel.insertShoppingList(list)
            viewModel.insertShoppingList(ShoppingList(isArchived = false))
            viewModel.insertShoppingList(ShoppingList(isArchived = false))

            viewModel.archiveShoppingList(list)

            val listsAfterArchiving = viewModel.currentLists.getOrAwaitValueTest()
            assertThat(listsAfterArchiving.size).isEqualTo(2)
        }
}